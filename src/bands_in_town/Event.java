/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bands_in_town;

import java.util.Date;
import java.util.TimeZone;
import java.text.SimpleDateFormat;
/**
 *
 * @author pro_Cion
 */
public class Event {
    private String ID;
    private Date date;
    private String url;
    private String place;
    private String city;
    private String country;
    
    public Event(String eID, Date eDate, String eUrl,
                 String ePlace, String eCity, String eCountry) {
        ID = eID;
        date = eDate;
        url = eUrl;
        place = ePlace;
        city = eCity;
        country = eCountry;
    }
    
    @Override
    public String toString() {
        SimpleDateFormat sdfLocal = new SimpleDateFormat("dd-MM-yyyy 'in' HH:mm:ss");
        sdfLocal.setTimeZone(TimeZone.getDefault());
        return "EventID: " + ID + "\nEvent place: " + place + "\nEvent Date: "+
                sdfLocal.format(date) + "\nPlace: "  + city + ", " + country + "\nWeb Page: " +
                url;      
    }
}
