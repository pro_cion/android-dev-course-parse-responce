/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bands_in_town;

import java.io.File;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.DataOutputStream;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.TimeZone;

import java.text.ParseException;
import java.text.SimpleDateFormat;
	 
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
	 
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author pro_Cion
 */

public class BandsInTown {

    // HTTP GET request
    private static void SendGet(String artist, String format_name) throws Exception {

            String url = "http://api.bandsintown.com/artists/";

            URL obj = new URL(url + artist + "/" + "events." + format_name);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            //con.setRequestProperty("User-Agent", "Mozilla/5.0");

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            BufferedWriter out = new BufferedWriter(new FileWriter("events.xml"));
            int tmp;
            while((tmp = in.read()) != -1) {
                out.write(tmp);
            }
            out.close();
            in.close();
    }
        
    public static void main(String[] args) throws ParserConfigurationException,
         SAXException, IOException, ParseException, Exception {
        
        SendGet("AmonAmarth","xml");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        Element venue_elem;
        String id, datetime, url,place_name, city, country;
        Date date_time;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        
        // Load the input XML document, parse it and return an instance of the
        // Document class.
        Document document = builder.parse(new File("events.xml"));

        List <Event> events = new ArrayList<>();
        NodeList nodeList = document.getDocumentElement().getChildNodes();        
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;

                // Get the value of all sub-elements.
                id = elem.getElementsByTagName("id").item(0)
                        .getChildNodes().item(0).getNodeValue();

                datetime = elem.getElementsByTagName("datetime").item(0)
                        .getChildNodes().item(0).getNodeValue();

                venue_elem = (Element)elem.getElementsByTagName("venue").item(0);

                url = venue_elem.getElementsByTagName("url").item(0)
                        .getChildNodes().item(0).getNodeValue();

                place_name = venue_elem.getElementsByTagName("name").item(0)
                        .getChildNodes().item(0).getNodeValue();

                city = venue_elem.getElementsByTagName("city").item(0)
                        .getChildNodes().item(0).getNodeValue();

                country = venue_elem.getElementsByTagName("country").item(0)
                        .getChildNodes().item(0).getNodeValue();

                
                date_time = formatter.parse(datetime);
                events.add(new Event(id, date_time , url, place_name, city, country));
                
            }
        }

        for (Event event : events) {
            System.out.println(event);
        }
    }
}